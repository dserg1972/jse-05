package com.nlmk.dezhemesov.taskmanager.constants;

/**
 * Константные значения для использования в консольном модуле
 */
public class Console {
    /**
     * Сведения об авторе
     */
    public static final String ABOUT = "about";
    /**
     * Помощь по ключам запуска
     */
    public static final String HELP = "help";
    /**
     * Сведения о версии
     */
    public static final String VERSION = "version";
    /**
     * Выход из программы
     */
    public static final String EXIT = "exit";
}
